


var color_pattern_schema = 

{
    "type":"object",
    "required":["type", "version", "color_array", "repeat_times"],
	"properties":{
		"type": {
			"type":"string"
		},
		"version": {
			"type":"number"
		},
		"color_array": {
			"type":"array",
			"items":
				{
					"type":"object",
					"required":["color", "sustain_time", "transit_time", "loopback_index", "loopback_times"],
					"properties":{
						"color": {
							"type":"object",
							"required":["red", "green", "blue", "white", "brightness"],
							"properties":{
								"red": {
									"type":"number",
									"minimum": 0,
									"maximum": 255
								},
								"green": {
									"type":"number",
									"minimum": 0,
									"maximum": 255
								},
								"blue": {
									"type":"number",
									"minimum": 0,
									"maximum": 255
								},
								"white": {
									"type":"number",
									"minimum": 0,
									"maximum": 255
								},
								"brightness": {
									"type":"number",
									"minimum": 0,
									"maximum": 255
								}
							}
						},

						"sustain_time": {
							"type":"number"
						},

						"transit_time": {
							"type":"number"
						},
						"loopback_index": {
							"type":"number"
						},
						"loopback_times": {
							"type":"number"
						}
					}
				}			

		},
		"repeat_times": {
			"type":"integer"
		}
	}
}
