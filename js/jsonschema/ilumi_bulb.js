

var processResult = function( data ){


		var errorMsg = "<div class=\"alert alert-error span6\">" +
	  		"<button type=\"button\" class=\"close\" data-dismiss=\"alert\">?—</button>" +
	  		"<strong>Warning!</strong> Better check your JSON.<br/><br/> ";


		errorMsg += data;

		$('#resultMessage').html( errorMsg + "</div>");

};


  var container = document.getElementById('jsoneditor');

  var options = {
    mode: 'code',
    change: function(){
        //$('.bulb-light').hide();
    }
  };

  var preload_json = 
{
    "type": "ilumi_color_patterns",
    "color_array": [
        {
            "index":0,
            "color": {
                "red": 255,
                "green": 0,
                "blue": 0,
                "white": 0,
                "brightness": 255
            },
            "sustain_time": 200,
            "transit_time": 0,
            "loopback_index": 0,
            "loopback_times": 0,

        },
        {
            "index":1,
            "color": {
                "red": 0,
                "green": 0,
                "blue": 0,
                "white": 0,
                "brightness": 0
            },

            "sustain_time": 200,
            "transit_time": 0,
            "loopback_index": 0,
            "loopback_times": 2,


        },
        {
            "index":2,
            "color": {
                "red": 0,
                "green": 0,
                "blue": 255,
                "white": 0,
                "brightness": 255
            },
            "sustain_time": 200,
            "transit_time": 0,
            "loopback_index": 0,
            "loopback_times": 0,
        },
        {
            "index":3,
            "color": {
                "red": 0,
                "green": 0,
                "blue": 0,
                "white": 0,
                "brightness": 0
            },
            "sustain_time": 200,
            "transit_time": 0,
            "loopback_index": 2,
            "loopback_times": 2,

        },
        {
            "index":4,
            "color": {
                "red": 255,
                "green": 0,
                "blue": 0,
                "white": 0,
                "brightness": 255
            },
            "sustain_time": 1000,
            "transit_time": 0,
            "loopback_index": 0,
            "loopback_times": 0,
        },
        {
            "index":5,
            "color": {
                "red": 0,
                "green": 0,
                "blue": 255,
                "white": 0,
                "brightness": 255
            },
            "sustain_time": 1000,
            "transit_time": 0,
            "loopback_index": 4,
            "loopback_times": 2,

        }
    ],
    "repeat_times": 1,
    "version" : 1.02
}

	var editor = new jsoneditor.JSONEditor(container, options, preload_json);
	var previewTimer;
 	var color_pattern_index = 0;
 	var repeat_times = 0;
 	var array_length = 0;
 	var json_color_pattern_string;

	function convertNumToTwoBytesHex(integer) { 
    	var str = Number(integer).toString(16); 
    	return str.length == 1 ? "0" + str : str; 
	};

	function to_css_rgb(r, g, b) { return "#" + convertNumToTwoBytesHex(r) + convertNumToTwoBytesHex(g) + convertNumToTwoBytesHex(b); }

	function RGBANumberToRGBHExString(r, g, b, a, intensity){
	    var r3 = Math.round(((1 - a) * r) + (a * 255))
	    var g3 = Math.round(((1 - a) * g) + (a * 255))
	    var b3 = Math.round(((1 - a) * b) + (a * 255))

	    //consider intensity, brightness
	    //r3 = Math.round( r3 * (intensity/255.0));
	    //g3 = Math.round( g3 * (intensity/255.0));
	    //b3 = Math.round( b3 * (intensity/255.0));

	    return to_css_rgb(r3, g3, b3); // return '#aabbcc'
    }

    /*
    * @param  colorDitctionray extracted from color pattern json input
    * @return rgb(0,255,255)
    */
	function colorDictionaryToRGB(colorDitctionray)
	{
		var r, g, b, w, brightness, alpha;
		r = colorDitctionray["red"];
		g = colorDitctionray["green"];
		b = colorDitctionray["blue"];
		w = colorDitctionray["white"];
		brightness = colorDitctionray["brightness"];

		alpha = w/255.0;


		var result = RGBANumberToRGBHExString(r, g, b, alpha, brightness);

		//console.log( result);
		return result
	}


	function bulb_set_color(color_string)
	{
		$( ".bulb-top" ).css( "background", color_string);
		$( ".bulb-bottom" ).css( "background", color_string);

		$( ".bulb-middle-1" ).css( "border-top", "55px solid " + color_string);
		$( ".bulb-middle-2" ).css( "border-top", "50px solid " + color_string);
		$( ".bulb-middle-3" ).css( "border-top", "30px solid " + color_string);

	}

    function bulb_set_transition_mode(mode)
    {
        if( mode === "LINEAR_64"){
            $( ".bulb-top" ).css( "-webkit-transition", "background 0.2s ease-in-out");
            $( ".bulb-top" ).css( "-moz-transition",    "background 0.2s ease-in-out");
            $( ".bulb-top" ).css( "-o-transition",      "background 0.2s ease-in-out");
            $( ".bulb-top" ).css( "transition",         "background 0.2s ease-in-out");

            $( ".bulb-bottom" ).css( "-webkit-transition", "background 0.2s ease-in-out");
            $( ".bulb-bottom" ).css( "-moz-transition",    "background 0.2s ease-in-out");
            $( ".bulb-bottom" ).css( "-o-transition",      "background 0.2s ease-in-out");
            $( ".bulb-bottom" ).css( "transition",         "background 0.2s ease-in-out");
         
            $( ".bulb-middle-1" ).css( "-webkit-transition", "border 0.2s ease-in-out");
            $( ".bulb-middle-1" ).css( "-moz-transition",    "border 0.2s ease-in-out");
            $( ".bulb-middle-1" ).css( "-o-transition",      "border 0.2s ease-in-out");
            $( ".bulb-middle-1" ).css( "transition",         "border 0.2s ease-in-out");

            $( ".bulb-middle-2" ).css( "-webkit-transition", "border 0.2s ease-in-out");
            $( ".bulb-middle-2" ).css( "-moz-transition",    "border 0.2s ease-in-out");
            $( ".bulb-middle-2" ).css( "-o-transition",      "border 0.2s ease-in-out");
            $( ".bulb-middle-2" ).css( "transition",         "border 0.2s ease-in-out");

            $( ".bulb-middle-3" ).css( "-webkit-transition", "border 0.2s ease-in-out");
            $( ".bulb-middle-3" ).css( "-moz-transition",    "border 0.2s ease-in-out");
            $( ".bulb-middle-3" ).css( "-o-transition",      "border 0.2s ease-in-out");
            $( ".bulb-middle-3" ).css( "transition",         "border 0.2s ease-in-out");
            
        }else if( mode === "LINEAR_32"){

            $( ".bulb-top" ).css( "-webkit-transition", "background 0.1s ease-in-out");
            $( ".bulb-top" ).css( "-moz-transition",    "background 0.1s ease-in-out");
            $( ".bulb-top" ).css( "-o-transition",      "background 0.1s ease-in-out");
            $( ".bulb-top" ).css( "transition",         "background 0.1s ease-in-out");

            $( ".bulb-bottom" ).css( "-webkit-transition", "background 0.1s ease-in-out");
            $( ".bulb-bottom" ).css( "-moz-transition",    "background 0.1s ease-in-out");
            $( ".bulb-bottom" ).css( "-o-transition",      "background 0.1s ease-in-out");
            $( ".bulb-bottom" ).css( "transition",         "background 0.1s ease-in-out");
         
            $( ".bulb-middle-1" ).css( "-webkit-transition", "border 0.1s ease-in-out");
            $( ".bulb-middle-1" ).css( "-moz-transition",    "border 0.1s ease-in-out");
            $( ".bulb-middle-1" ).css( "-o-transition",      "border 0.1s ease-in-out");
            $( ".bulb-middle-1" ).css( "transition",         "border 0.1s ease-in-out");

            $( ".bulb-middle-2" ).css( "-webkit-transition", "border 0.1s ease-in-out");
            $( ".bulb-middle-2" ).css( "-moz-transition",    "border 0.1s ease-in-out");
            $( ".bulb-middle-2" ).css( "-o-transition",      "border 0.1s ease-in-out");
            $( ".bulb-middle-2" ).css( "transition",         "border 0.1s ease-in-out");

            $( ".bulb-middle-3" ).css( "-webkit-transition", "border 0.1s ease-in-out");
            $( ".bulb-middle-3" ).css( "-moz-transition",    "border 0.1s ease-in-out");
            $( ".bulb-middle-3" ).css( "-o-transition",      "border 0.1s ease-in-out");
            $( ".bulb-middle-3" ).css( "transition",         "border 0.1s ease-in-out");
            
        }else{
            $( ".bulb-top" ).css( "-webkit-transition", "");
            $( ".bulb-top" ).css( "-moz-transition",    "");
            $( ".bulb-top" ).css( "-o-transition",      "");
            $( ".bulb-top" ).css( "transition",         "");

            $( ".bulb-bottom" ).css( "-webkit-transition", "");
            $( ".bulb-bottom" ).css( "-moz-transition",    "");
            $( ".bulb-bottom" ).css( "-o-transition",      "");
            $( ".bulb-bottom" ).css( "transition",         "");
         
            $( ".bulb-middle-1" ).css( "-webkit-transition", "");
            $( ".bulb-middle-1" ).css( "-moz-transition",    "");
            $( ".bulb-middle-1" ).css( "-o-transition",      "");
            $( ".bulb-middle-1" ).css( "transition",         "");

            $( ".bulb-middle-2" ).css( "-webkit-transition", "");
            $( ".bulb-middle-2" ).css( "-moz-transition",    "");
            $( ".bulb-middle-2" ).css( "-o-transition",      "");
            $( ".bulb-middle-2" ).css( "transition",         "");

            $( ".bulb-middle-3" ).css( "-webkit-transition", "");
            $( ".bulb-middle-3" ).css( "-moz-transition",    "");
            $( ".bulb-middle-3" ).css( "-o-transition",      "");
            $( ".bulb-middle-3" ).css( "transition",         "");
        }
    }

	function reset_variables()
	{
		color_pattern_index = 0;
		repeat_times = 0;
		array_length = 0;
		previewTimer = undefined;
		$('#preview_button').html('Preview');
		bulb_set_color("#E7E7E7");
	}

	function move_to_next_color_scheme()
	{
		color_pattern_index = (color_pattern_index+1) % array_length;//point to next

    	if (color_pattern_index == 0 ){
    		repeat_times++
    		console.log("B) repeat_times  %d", repeat_times);
		}
	}

	/*
	* @brief convert json to C struct 
	*/
	function generate_expereince_byte_array()
	{
		var byte_array = [];
		var new_json = editor.get();
		var color_scheme_bytes_size = new_json["color_array"].length * 18;
		var offset = 0;
		byte_array[offset++] = 0xCE;
		byte_array[offset++] = 0xFA;

        byte_array[offset++] = 0; //simple check sum, just add all bytes together
        byte_array[offset++] = 0; //for all bytes after check sum field

		byte_array[offset++] = new_json["repeat_times"];
		byte_array[offset++] = 0;                         //next_scene_idx


		byte_array[offset++] = color_scheme_bytes_size & 0xff;
		byte_array[offset++] = (color_scheme_bytes_size >> 8);

        //size = 18 * # of color schemes
		for(i = 0; i< new_json["color_array"].length; i++){

			//sturct color newColor;
			byte_array[offset++] = json_color_pattern_string["color_array"][i]["color"]["red"];
			byte_array[offset++] = json_color_pattern_string["color_array"][i]["color"]["green"];
			byte_array[offset++] = json_color_pattern_string["color_array"][i]["color"]["blue"];
			byte_array[offset++] = json_color_pattern_string["color_array"][i]["color"]["white"];
			byte_array[offset++] = json_color_pattern_string["color_array"][i]["color"]["brightness"];
			byte_array[offset++] = 0; //reserved

			var sustain_time = json_color_pattern_string["color_array"][i]["sustain_time"];//32bit
            byte_array[offset++] = (sustain_time >>  0) & 0xff;
            byte_array[offset++] = (sustain_time >>  8) & 0xff;
            byte_array[offset++] = (sustain_time >> 16) & 0xff;
            byte_array[offset++] = (sustain_time >> 24) & 0xff;

            var transit_time = json_color_pattern_string["color_array"][i]["transit_time"];//32bit
            byte_array[offset++] = (transit_time >>  0) & 0xff;
            byte_array[offset++] = (transit_time >>  8) & 0xff;
            byte_array[offset++] = (transit_time >> 16) & 0xff;
            byte_array[offset++] = (transit_time >> 24) & 0xff;

            byte_array[offset++] = 0; //sustain_effect 8bit
            byte_array[offset++] = 0; //transit_effect 8bit
            byte_array[offset++] = json_color_pattern_string["color_array"][i]["loopback_index"];//8bit
            byte_array[offset++] = json_color_pattern_string["color_array"][i]["loopback_times"];//8bit

		}

        var check_sum = 0;
        for(i = 4; i< byte_array.length; i++){
            check_sum = check_sum + byte_array[i];
        }

        byte_array[2] = check_sum & 0xff;                //filled in check_sum field
        byte_array[3] = (check_sum >> 8) & 0xff;

        return byte_array;
	}

	function scene_timer_routine()
	{

    	if (color_pattern_index == 0 ){ //check whether we repeat enough times
    		
    		if( json_color_pattern_string["repeat_times"] ){
    			if ( repeat_times > json_color_pattern_string["repeat_times"] ){
    				console.log("Repeat enough times");
    				reset_variables();
    				return;
    			}
    		}else{
    			//did not define repeattimes, quit 
    			console.log("No need to repeat");
    			reset_variables();
    			return;
    		}    		
    	}


    	// set color pointer by current index
    	var color_patter_array = json_color_pattern_string["color_array"];

    	var current_color = color_patter_array[color_pattern_index]["color"];

    	var next_color = colorDictionaryToRGB(current_color);

        //set transition mode
        bulb_set_transition_mode(  color_patter_array[color_pattern_index]["transit"]  );
    	bulb_set_color(next_color);
    	console.log("Set color for index %d", color_pattern_index);

    	var next_color_time_interval = color_patter_array[color_pattern_index]["sustain_time"];


    	if ( next_color_time_interval ){
    		//console.log("Schedule next");
    		
    		previewTimer = setTimeout(scene_timer_routine, next_color_time_interval );
    	}else{
    		console.log("  next_color_time_interval is 0 ");
    	}

    	//placeholder whethre loopback repeat
    	if ( color_patter_array[color_pattern_index]["loopback_times"] && color_patter_array[color_pattern_index]["loopback_index"] < array_length )
    	{ 
            //whether we have already have a loop counter
            if ( color_patter_array[color_pattern_index]["loop_counter"] ){
                if (color_patter_array[color_pattern_index]["loop_counter"] < color_patter_array[color_pattern_index]["loopback_times"] ){
                    color_patter_array[color_pattern_index]["loop_counter"]++;             

                    console.log("Loop to %d counter %d", color_pattern_index, color_patter_array[color_pattern_index]["loop_counter"] )
                    color_pattern_index = color_patter_array[color_pattern_index]["loopback_index"];
                }else{
                    console.log("Loop enough");
                    color_patter_array[color_pattern_index]["loop_counter"] = 0;
                    move_to_next_color_scheme();
                }

            }else{
                //No loopcounter for this loop yet, first time         
                color_patter_array[color_pattern_index]["loop_counter"] = 1;
                console.log("First Loop to %d counter %d", color_pattern_index, color_patter_array[color_pattern_index]["loop_counter"] )
                color_pattern_index = color_patter_array[color_pattern_index]["loopback_index"];
            }
    	}else{
    		move_to_next_color_scheme();
    	}
	}

    $(document).ready(function() {
        $('#resultMessage').hide();

        $('#preview_button').click(function() {
        	
        	if ( previewTimer ) {
        		//already started preview, trigger stop
        		clearTimeout(previewTimer);
        		reset_variables();
        	}else{
        		$(this).html('Stop');
        		json_color_pattern_string = editor.get();
        		array_length = json_color_pattern_string["color_array"].length;
        		scene_timer_routine();
        	}

        });

        $('#validate_button').click(function() {
            var new_json = editor.get();
            if (  tv4.validate(new_json, color_pattern_schema) ){
            	$('#resultMessage').show();
            	setTimeout(function(){
        			$('#resultMessage').hide(500);
    				}, 2000);
                $('.bulb-light').show();
            }else{
                alert("json error: " + JSON.stringify(tv4.error, null, 4));
            }
        });

        //save json and color patterns to file
        $('#SaveButton').click(function(event) {
			event.preventDefault();
			

			//A) Save json text file
			// var new_json_text = editor.getText();
			// saveAs(
			// 	  new Blob(
			// 		  [new_json_text]
			// 		, {type: "text/plain;charset=" + document.characterSet}
			// 	)
			// 	, $("#text-filename").val() + ".json"
			// );

			//B) save color pattern binary file

        	var blob_data = generate_expereince_byte_array();
        	
			saveAs(
				new Blob(
					[new Uint8Array(blob_data)],
					{type: "application/octet-stream"}
				)
				, $("#text-filename").val() + ".icp"
			);


        });

    });
