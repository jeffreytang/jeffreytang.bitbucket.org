


var ilumi_experience_schema = 

{
    "type":"object",
    "required":["version", "color_pattern_command_array", "repeat_times"],
	"properties":{
		"version": {
			"type":"number"
		},
		"required_node_number": {
			"type":"object",
			"properties":{
				"fix_number" : "number",
				"odd_number" : "boolean",
				"even_number" : "boolean",
				"times_of_number" : "number"
			}
		},
		"color_pattern_command_array": {
			"type":"array",
			"items":{
				"type":"object",
				"properties":{
					"target_node_id": {
						"type":"number"
					},
					"color_pattern_data"{
						"schema" : "schema link reference"
					}
				}
			}
		}

	}
}
