#Basics

[iLumi Solutions](http://ilumi.co/) has defined customized GATT profile for BLE (Bluetooth Low Energy) enabled intelligent LED bulbs, any BLE enabled device (iOS/Android/Blackberry/Windows 8/etc) can send API function calls to iLumi LED bulbs by writing an array of byte data to a predefined GATT characteristics.

All API functions mentioned in the document are not specific to iOS, Android or any specific platforms. Any BLE enabled device can use the same API functions to control and communicate with iLumi bulbs.

Before issuing API function calls to an iLumi LED bulb, your device need to discover iLumi bulb first, then make the connection through platform specific function calls. For example, if you are using an iOS device, you need to use following functions from CoreBluetooth to communicate with iLumi bulbs

    scanForPeripheralsWithServices
    didDiscoverPeripheral
    connectPeripheral
    didConnectPeripheral
    discoverServices
    didDiscoverService
    discoverCharacteristics
    didDiscoverCharacteristicsForService
	setNotifyValue
	writeCharacteristic

iLumi Solutions provides iOS SDK to encapsulate all BLE functions to ease the development on iOS devices. Next, we introduce a few concepts we used to define the visual effect of LED bulbs.

##Color
A color data structure is the combination of four color elements (Red, Green, Blue, White) and the brightness (Intensity). Its definition in C structure looks like this:

    typedef struct{
	    uint8_t R;
	    uint8_t G;
	    uint8_t B;
	    uint8_t W;
        uint8_t Brightness;
        uint8_t reserved;
    } color


##Color Scheme
A color scheme is a color with timing information (how long the color would sustain) and transition effects (how to change to another color from current color). There are two kinds of transition effects. The first one is the transition between two colors, for example, when changing from RED to GREEN, you can have a ***snap*** (instantaneously) transition, a ***smooth*** transition, or a ***very slow*** transition take a few seconds or minutes. The second effect is used when there is no color changes but you what some pulsing (keeps dimming in and dimming out) or strobe effects (quick flashing) with current color.

The data structure of a color scheme in C looks like this:   

    struct  {
        struct color newColor;  /**< the color set to iLumi bulb             */
        uint16_t  time;         /**< the time period to keep the color       */
        uint8_t   unit;         /**< the time unit in 0 = millisecond, 1 = second, 2 = minute, 3 = hour   */
        uint8_t   effect;       /**< the transition and animation effects between two color schemes   */
        uint8_t   pulse_effect; /**< effects during the color is on, not during two color transition  */
        uint8_t   reserved;
    };

Please note that its size in bytes is 12.

We have predefined some transition effects in the iLumi firmware and also would support mechanism to allow user to define customized transition effect (sine/cosine transition, logarithmic transition, etc). 



##Scenes (Color Patterns)
A scene (we use **scene** and **color pattern** interchangeably in this document) is simply an array of color schemes. For example, a typical police siren is a series of red, blue colors, and you can define it as a color scene: 500ms red, snap change to blue, blue sustains for 500ms, changes to fast flash of red, then changes to fast flash of blue. A scene can also be repeated for a user defined time period, or concatenated together to form more complicated visual experience. 

For example, the end of one scene would trigger the start of another scene.

##Controller
A controller is any BLE-enabled device, either a smart phone, tablet or PC than can communicate with iLumi bulb through BLE protocols

##Commissioning 
Commissioning is the process of writing a 32-bit network key into iLumi device during the pairing process. By default, any controller can issue API command to iLumi bulb. Once a controller writes a 32-bit network key to the iLumi bulb, the controller becomes "the master" or "the owner" of the iLumi bulb.
 
After a iLumi bulb is commissioned, it only accepts an API command with the correct network key which matches the one configured by the controller. In other words, no other controller can control the iLumi unless it knows or shares the 32-bit network key.


#API calls
The controller sends API command to iLumi bulb by writing an array of byte data to a GATT characteristics. The array of byte data can be treated as an data packet, and it always starts with 32-bit network key then followed by one byte of message type.


The message structure used for iLumi API calls are presented in data packet: the first row is from Byte 0 to Byte 3, the second row is from Byte 4 to Byte 7, etc.

    insert byte table here

In the following sections, only the first row Byte 0 to Byte 3 is shown as the table header for each message structure.

[Little endian](http://en.wikipedia.org/wiki/Endianness) is used for bytes ordering. For example, when putting a 16-bit number 0xABCD into byte array, 0xCD will be byte 0 and 0xAB will be byte 1.
In addition, an example of a 32-bit network key (0xdeadbeef) is shown as below. 

    insert 32bit example table here


##Basic Color Controls

###Set color
The command of setting color is simply sending six bytes of data defined in **struct Color**. The color set by this API call is **NOT** saved in iLumi bulb. Once iLumi bulb reset or lost power, the configuration is gone.

    insert table here
 
###Set default color
Setting default color has exactly the same packet format as `set color` except the different message type. The default color is the color when iLumi bulb powers on and it is saved to non-volatile memory therefore even after a power loss the iLumi bulb still remembers its default color.

    insert table here
 
###Blink
Blink API call enables iLumi to change color between black (bulb off) and the user defined color. `time` defines the interval, `unit` defines time unit, `repeat_times` defines how many times iLumi bulb should blink.

    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
        time_unit unit;
        uint16_t time;
        struct color new_color;
        uint8_t repeat_times;
    } gatt_ilumi_blink_t;

###Blink with current color
Blink with default color is quite the same is `Blink` except no color need to be defined and iLumi bulb just blinks with its current color 

###Turn on bulb
Nothing fancy here, just set the message type and iLumi bulb would be off.

###Turn of bulb
Nothing fancy here, just set the message type and iLumi bulb would be **ON** with its **default color**.

###Set color smooth
This API set a new color with smooth transition. The above set color API changes the color instantly.

 `time` defines the interval, how long to gradually change to the new color

 `unit` defines time unit

    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
        time_unit unit;
        uint16_t time;
        struct color new_color;
    } gatt_ilumi_set_color_smooth_t;

For example, if the color is set to change from all zero (black) to all white within a one hour period, then iLumi can mimic the process of the sun rise 

##Get current color
Returns four bytes of color information:Red, Green, Blue and White(Brightness).

##Scenes and Color patterns
###Set color patterns

`scene_idx` : an index number assigned to the color pattern so it can be referenced later.

`array_size` : the number of color scheme in this scene.

`repeatable`: how many times to repeat the color pattern. iLumi will starts showing the color of first color scheme, after the defined time internal it shows the the color of second color scheme. After reaching the last color scheme, iLumi may back to the first color scheme and repeat the color pattern again. A value of 0xFF means to repeat forever until it receives an "Scene Stop" API call.

`next_idx` : the next scene index number after the current scene is finished. This field enables user to define much complicated visual effects by chaining multiple scenes together.

`payload_size`: the total length in bytes of this API message, starts from 32-bit network key until the end of the last color scheme.  

`start` : a non-zero value in `start` enables iLumi to start the scene immidiatley. Otherwise, an API has to specifically call `Start Scene` API to start the scene.


    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
        uint8_t scene_idx;
        uint16_t payload_size; //including everything even this header
        uint8_t array_size;
        uint8_t repeatable;
        uint8_t next_idx;
        uint8_t start;
    } gatt_ilumi_set_scene_t;

###Start color pattern
Telling iLumi bulb which scene to start with

    insert table here

###Delete a color pattern

`scene_idx` : an index number of the scene to be deleted.
 
###Delete all color patters

###Set candle mode
This API sets iLumi bulb into candle mode: the beautiful flickering light!

    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
        struct color new_color;
    } gatt_ilumi_set_color_t;

##Alarms
Currently only maximum 16 alarms are supported. Each alarm has a index number in the range from 0 to 15.

###BCD Time
**All time and date fileds used in alarm related API calls are in [BCD format](http://en.wikipedia.org/wiki/Binary-coded_decimal)**.

A six-byte data format is used by present a combination of date and time, where the first byte is second, the second byte is minute, etc, as indicated in the following C structure.

Be careful that with different computer systems and compilers, the actual size of the previously showed data structure could be 6 bytes or 8 bytes. To be able to send correct date/time information to iLumi bulb, the data structure needs to be 6 bytes, no less and no more. Some compilers support compiler directives such as `packer(1)` or `#program packed`, please make sure you have the right size especially when you are going to copy the value of a BCD time member to or from an API message.


    struct  bcd_time{
        uint8_t second;// BCD format, valid value 0x00~0x59
        uint8_t minute;// BCD format, valid value 0x00~0x59
        uint8_t hour;  // BCD format, valid value 0x00~0x23
        uint8_t day;   // BCD format, valid value 0x01~0x31
        uint8_t month; // BCD format, valid value 0x01~0x12
        uint8_t year;  // BCD format, valid value 0x00~0x99
    };

###Set date and time

Before setting an alarm on iLumi bulb, you must set data and time first. This API call simply writes the 6 byte date and time information into the iLumi bulb. iLumi bulb has it own button battery therefore even after a power lost it internal clock continue to run and maintain the date and time. But the internal crystal oscillator of iLumi bulb is not a high resolution one therefore it is encourage to synchronize the date and time between controller and iLumi bulb more frequently, such as every few days or weeks. 
 

    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
        struct bcd_time new_time;
    } gatt_ilumi_set_data_time_t;

###Get date and time
The API call reads the current date and time from iLumi bulb. iLumi bulb sends back the result through GATT notification of iLumi API characteristics. 

    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
    } gatt_ilumi_general_t;


###Turn off alarm (Scene Stop)
Stop the current active scene and restore the color before the scene starts

###Set daily repeatable alarm

This API call set a repeatable alarm that triggers at user defined time.

`alarm_idx`: the index reference number of the alarm.

`callback_scene_idx`: when the alarm triggers, which scene to start with.

`hour` and `minute`: the time the alarm should trigger. For example, if `hour` is 0x18 and `minute` is 0x30, then the alarm triggers at 6:30 PM.

`weekdays` is a bitmask that defines which weekday or weekend the alarm should trigger. Bit 0 is Sunday, Bit 1 is Monday, Bit 2 is Tuesday, ..., and Bit 6 is Saturday. For example, if `weekdays` is 0x3E (binary 0011.1110), then alarm only triggers from Monday to Friday.

    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        
        uint8_t  message_type;
        uint8_t alarm_idx;
        uint8_t callback_scene_idx;
        
        uint8_t hour;
        
        uint8_t minute;
        uint8_t weekDays;
    }gatt_ilumi_daily_alarm_t;

###Set calendar event
This API call sets a start date/time, an end date/time, and the interval of a repeatable alarm. The internal can be any combination of month, day, hour, or minutes.

`alarm_idx`: the index reference number of the alarm.

`callback_scene_idx`: when the alarm triggers, which scene to start with.

 `interval` defines the interval.

 `time_unit` defines time unit.

`startTime` is the date and time you want the alarm becomes effective

`endTime` is the date and time you want the alarm becomes expire.

For example, if the start time is 09/30/2013 18:30PM, `time_unit` is HOURS and `interval` is 2, then every two hours the alarm would fire; the first firing time is 18:30PM on 09/30/2013, then the following firing time is 20:30PM, 22:30PM, etc.


    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        
        uint8_t  message_type;
        uint8_t alarm_idx;
        uint16_t reserved1; 
        
        uint8_t callback_scene_idx;
        uint8_t time_unit;
        
        uint8_t interval;
        uint8_t reserved2;   
        struct bcd_time startTime;
        struct bcd_time endTime;
    }gatt_ilumi_calendar_event_t;

###Set Nth Day Event Alarm
This API call is used to set monthly repeatable alarm, can be quite helpful for setting a reminder such as paying your rent or utility bill.


`alarm_idx`: the index reference number of the alarm.

`callback_scene_idx`: when the alarm triggers, which scene to start with.

`NthDay`: defines on which day of every month the alarm should trigger. With the value of 2, the alarm triggers on 2nd of every month. With value of -1 (hex value 0xFF) the alarm triggers on the last day of each month 

`startDateAndAlarmTime` is the date and time you want the alarm becomes effective. The HOUR and MINUTE field is the time that alarm should trigger. For example, if you set `NthDay` as -1 and `startDateAndAlarm` time as 09/30/2013 18:30PM, then it triggers at 18:30PM of the last day of every month, and its next trigger time is 10/31/2013 18:30PM.

`endDate` is the date and time you want the alarm becomes expire.


    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        
        uint8_t  message_type;
        uint8_t alarm_idx;
        uint16_t reserved1; 
        
        uint8_t callback_scene_idx;
        int8_t NthDay;
        
        struct bcd_time startDateAndAlarmTime;
        struct bcd_time endDate;
    }gatt_ilumi_nth_day_event_t;

###Get next alarm time

Returns a six-byte BCD time which is the next alarm firing time on iLumi bulb.

###Get current active event

When iLumi starts strobing or blinking due to an alarm setting, user probably forgets which alarm triggers the scene. This API return the one byte alarm index number.

###Delete Alarm

`alarm_idx`: the index number of the alarm to be deleted

    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
        uint8_t  alarmIdx;
    } gatt_ilumi_del_alarm_t;

###Delete All Alarms



    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
    } gatt_ilumi_del_alarm_t;



##Mesh Networking

##Group and user management
###Assign node ID
Assign a two-byte node ID to iLumi bulb
###Assign group ID

Assign group IDs to iLumi bulb. Each iLumi bulb can belong to multiple (**maximum 8**) group at the same time. 

###Delete all group IDs
###Set Network Key
Set a 32-bit network key for commissioning purpose.

    typedef struct __PACKED_STRUCT_BEGIN__{
        uint32_t network_key;
        uint8_t  message_type;
        uint8_t  resverd;        //for 16bit 32bit alignment purpose
        uint32_t new_network_key;
    } gatt_ilumi_set_network_key_t;


###Read node ID
Returns a two-byte node ID previously assigned.
###Read group IDs
returns an array of two-byte which are previously assigned group IDs.


###Delete all user data
Delete all user defined colors, color patterns, alarms, 32-bit network key, restore to manufacturer default.

##System management

###Read firmware version
Returns a two-byte firmware version
###Read bootloader version
Returns a two-byte bootloader version



#References
##Read result

iLumi bulb responds read-related API calls, such as get current color or get current time, by sending a GATT notification to controller. The result is a byte array. First two bytes indicate the size of reading result. The following byte indicate response type, which is the same as the API call that triggers the notification. Payload field is a byte array with the lenght of `data_size`.

Its C data structure looks like this:

    typedef struct {
        uint16_t data_size;
        uint8_t  msg_type;
        uint8_t payload [];
    } api_read_result_t;
    


##iLumi UUID
###UUID of iLumi Service
This is UUID required by BLE-enabled controller to scan for BLE peripherals.



> F000F0C0-0451-4000-B000-000000000000

###UUID of iLumi API characteristics
This is UUID used to identify GATT characteristics of iLumi API

> F000F0C1-0451-4000-B000-000000000000

##iLumi Error Code

##Time Unit

    typedef enum  __PACKED_ENUM_TYPE__ {
        TIME_UNIT_MILISECOND,
        TIME_UNIT_SECOND,
        TIME_UNIT_MINUTE,
        TIME_UNIT_HOUR,
        TIME_UNIT_DAY,
        TIME_UNIT_WEEK,
        TIME_UNIT_MONTH,
        TIME_UNIT_YEAR,
    } time_unit;

#Examles
Examples shown here are simple demos without considering better software architecture or engineering practices. Ideally, the software module responsible for BLE or iLumi API call should be separated from business logic and UI controls.


##iOS Example
##Android Example

#SDK
## iOS SDK
To be developed.
## Android SDK
To be developed.